# Wrap-up y Cierre

#

## Retrospectiva

Escribe dos post-it como respuesta a cada una de estas preguntas:

> **¿Qué conceptos destacaríais?**

> **¿Qué ves aplicable en tu entorno? ¿Y qué no?**

> **A nivel personal... ¿cómo seguirás desarrollándote?** 

> **¿Qué te ha gustado del curso? ¿Y qué cambiarías?**
