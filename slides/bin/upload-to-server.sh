#!/bin/bash

# default global_opts
exec_root_dir=$(dirname $0)/..
host=devops-training.duckdns.org
#host=192.168.102.225
port=44044
#port=22
user=devops-slides-ci


# main()

cd $exec_root_dir

sftp -P $port $user@$host -b <<EOF                                 
cd html
mput html/*.html
EOF

