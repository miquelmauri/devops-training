from troposphere import Base64, FindInMap, GetAtt, GetAZs, Join, Output
from troposphere import Parameter, Ref, Template
import troposphere.ec2 as ec2
import troposphere.elasticloadbalancing as elb
from troposphere.cloudformation import Init, InitFile, InitFiles, \
    InitConfig, InitService, InitServices
from troposphere.autoscaling import AutoScalingGroup, Tag
from troposphere.autoscaling import LaunchConfiguration
from troposphere.policies import (
    AutoScalingReplacingUpdate, AutoScalingRollingUpdate, UpdatePolicy
)
from troposphere import autoscaling
import argparse


def generate_template_json(params):
    t = Template()

    t.add_description("AWS CloudFormation Sample Template: ELB with ASG")

    t.add_mapping("RegionMap", {
            "eu-west-1": {"AMI": "ami-01ccc867"},
            "eu-central-1": {"AMI": "ami-b968bad6"}
        })

    scale_capacity = t.add_parameter(Parameter(
        "ScaleCapacity",
        Default=params['ScaleCapacity'],
        Type="String",
        Description="Number of APP servers to run",
    ))

    keyname_param = t.add_parameter(Parameter(
        "KeyName",
        Type="String",
        Default=params['Keyname'],
        Description="Name of an existing EC2 KeyPair to "
                    "enable SSH access to the instance",
    ))

    t.add_parameter(Parameter(
        "InstanceType",
        Type="String",
        Description="WebServer EC2 instance type",
        Default="t2.micro",
        AllowedValues=[
            "t2.micro", "t2.small", "t2.medium",
            ],
        ConstraintDescription="must be a valid EC2 instance type.",
    ))

    webport_param = t.add_parameter(Parameter(
        "WebServerPort",
        Type="String",
        Default=params['WebServerPort'],
        Description="TCP port of the web server",
    ))

    docker_image = t.add_parameter(Parameter(
        "DockerImage",
        Type="String",
        Default=params['DockerImage'],
        Description="Docker Image to use",
    ))

    instance_sg = t.add_resource(
        ec2.SecurityGroup(
            "InstanceSecurityGroup",
            GroupDescription="Enable SSH and HTTP access on the inbound port",
            SecurityGroupIngress=[
                ec2.SecurityGroupRule(
                    IpProtocol="tcp",
                    FromPort="22",
                    ToPort="22",
                    CidrIp="0.0.0.0/0",
                ),
                ec2.SecurityGroupRule(
                    IpProtocol="tcp",
                    FromPort=Ref(webport_param),
                    ToPort=Ref(webport_param),
                    CidrIp="0.0.0.0/0",
                ),
            ]
        )
    )

    # Add Autoscaling group
    LaunchConfig = t.add_resource(LaunchConfiguration(
        "LaunchConfiguration",
        Metadata=autoscaling.Metadata(
            Init({
                'config': InitConfig(
                    packages={'yum': {'docker': []}},
                    commands={
                        'run_docker': {
                            'command': Join('', [
                                'sudo service docker start; ',
                                'sudo docker run -d -p ',
                                Ref(webport_param),
                                ':',
                                Ref(webport_param),
                                ' ',
                                Ref(docker_image)
                            ])
                        },
                    }
                )})
        ),
        UserData=Base64(Join('', [
            "#!/bin/bash -xe\n",
            "yum update -y aws-cfn-bootstrap\n",
            "/opt/aws/bin/cfn-init -v ",
            "         --stack ", {"Ref": "AWS::StackName"},
            "         --resource LaunchConfiguration ",
            "         --region ", Ref("AWS::Region"), "\n",
            "/opt/aws/bin/cfn-signal -e $? ",
            "         --stack ", {"Ref": "AWS::StackName"},
            "         --resource AutoscalingGroup ",
            "         --region ", Ref("AWS::Region"), "\n",
        ])),
        SecurityGroups=[Ref(instance_sg)],
        KeyName=Ref(keyname_param),
        InstanceType=Ref("InstanceType"),
        ImageId=FindInMap("RegionMap", Ref("AWS::Region"), "AMI"),
    ))

    elasticLB = t.add_resource(elb.LoadBalancer(
        'ElasticLoadBalancer',
        AvailabilityZones=GetAZs(""),
        ConnectionDrainingPolicy=elb.ConnectionDrainingPolicy(
            Enabled=True,
            Timeout=300,
        ),
        CrossZone=True,
        Listeners=[
            elb.Listener(
                LoadBalancerPort="80",
                InstancePort=Ref(webport_param),
                Protocol="HTTP",
            ),
        ],
        HealthCheck=elb.HealthCheck(
            Target=Join("", ["HTTP:", Ref(webport_param), "/"]),
            HealthyThreshold="3",
            UnhealthyThreshold="5",
            Interval="120",
            Timeout="30",
        )
    ))

    t.add_resource(AutoScalingGroup(
        "AutoscalingGroup",
        DesiredCapacity=Ref(scale_capacity),
        LaunchConfigurationName=Ref(LaunchConfig),
        MinSize=Ref(scale_capacity),
        MaxSize=Ref(scale_capacity),
        LoadBalancerNames=[Ref(elasticLB)],
        AvailabilityZones=GetAZs(""),
        HealthCheckType="EC2",
        UpdatePolicy=UpdatePolicy(
            AutoScalingReplacingUpdate=AutoScalingReplacingUpdate(
                WillReplace=True,
            ),
            AutoScalingRollingUpdate=AutoScalingRollingUpdate(
                PauseTime='PT5M',
                MinInstancesInService="1",
                MaxBatchSize='1',
                WaitOnResourceSignals=True
            )
        )
    ))

    t.add_output(Output(
        "URL",
        Description="URL of devops demo",
        Value=Join("", ["http://", GetAtt(elasticLB, "DNSName")])
    ))

    print(t.to_json())


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--scale-capacity',
                        default='1',
                        help='How many instances will be running')
    parser.add_argument('--keyname',
                        default='chadell_aws',
                        help='The SSH key deployed in the instances')
    parser.add_argument('--docker-image',
                        default='devops_docker',
                        help='Container version to be deployed')
    parser.add_argument('--webport',
                        default='5000',
                        help='TCP port of the web server')

    args = parser.parse_args()
    params = {
        'ScaleCapacity': args.scale_capacity,
        'Keyname': args.keyname,
        'DockerImage': args.docker_image,
        'WebServerPort': args.webport,
    }

    generate_template_json(params)
