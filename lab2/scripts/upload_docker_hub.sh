#!/bin/bash

set -ex

docker build -t $BITBUCKET_REPO_SLUG:latest app/.

docker login -u $DOCKERHUBUSER -p $DOCKERHUBPASS

docker tag $BITBUCKET_REPO_SLUG $DOCKERHUBUSER/$BITBUCKET_REPO_SLUG:latest

docker push $DOCKERHUBUSER/$BITBUCKET_REPO_SLUG:latest

