#

## SCM (Software Configuration Management)

<!-- https://sewiki.iai.uni-bonn.de/_media/teaching/labs/xp/2012b/seminar/6-scm.pdf -->

SCM cubre todas las prácticas y mecanismos para tener **control sobre los cambios** software de los diferentes componentes que pueden formar parte de un sistema software

> Nota: no confundir con gestión de configuración de un servicio y herramientas para distribuir configuraciones

## Control de versiones

SCM se apoya fuertemente en el uso de sistemas de control de versiones (VCS).
**Git** se ha posicionado como el estándar de facto (reemplazando CVS y SVN). Destaca por:

* Estar diseñado para la alta concurrencia y volumen
* Romper el modelo "centralizado" de sus ancestros ("distributed SCM")
    * Cada desarrollador tiene una copia local del repositorio, que evoluciona y eventualmente se "mergea" con un repositorio "central"
    * No se requiere pues conectividad permanente para "commitear" nuevas versiones
    * Las operaciones más habituales se pueden completar más rápido (se trabaja, por defecto, en local)
* Han surgido repositorios públicos que han abanderado una nueva forma de contribuir a proyectos Open Source
    * Larga vida a sourceforge / freshmeat-freecode

## Modelos - estrategias de "branching" (ó workflows)

<!-- https://www.atlassian.com/git/tutorials/comparing-workflows -->

Los sistemas de control de versiones ofrecen varias funcionalidades ó primitivas que permiten diseñar modelos sobre como gestionar versiones / releases / features / parches, destacando:

* **Commits**: unidad que representa un cambio, "diff"
* **Branches**: Ramas; copias del estado del "repositorio" en un momento, que pasan a tener su ciclo de vida
* **Tags**: "Puntos de control/Etiquetas", podríamos verlo como "ramas estáticas"

## Modelos (1): Múltiples "masters"

> Master ~= Trunk

<!-- https://www.md-systems.ch/en/blog/techblog/2012/04/25/git-branching-model -->

![](git-workflow.png){width=60%}


## Modelos (2): Feature branch

<!-- https://buddy.works/blog/5-types-of-git-workflows -->

Los modelos de Continuous Delivery empujan a la simplicidad

* Los **mecanismos de protección** vienen dados por una buena cobertura automatizada de tests (en lugar de una secuencia de integración de branches)
* El trigger para desplegar deja de ser el "tag" para ser la **integración a master / commit / build**

![](https://buddy.works/data/blog/_images/feature-branch.png)

## Modelos (3): Trabajando con Github - Forking

* En Github, los repositorios tienen ownership, y no cualquier contributor puede crear branches
* En su lugar, es común que un potencial contributor haga un **fork** (clon) del repositorio original (upstream) y trabaje sobre él
* Los forks alimentan al repositorio original via **Pull Requests**

##

<!-- https://github.com/satellite-game/Satellite/wiki/Git-Workflow -->

![](https://camo.githubusercontent.com/acda699e5284801700a2da9e56cf4a2a95e45b39/68747470733a2f2f662e636c6f75642e6769746875622e636f6d2f6173736574732f313537373638322f313931333431382f39663332656330302d376434382d313165332d383538642d6235356336633439613662302e706e67){width=70%}

<!--

## Git

```bash
0. Instalar Git

1. Clonar un proyecto
    git clone [url]

2. Verificar los cambios pendientes
    git status

3. Añadir código a versionar
    git add [file]

4. Guardar el código añadido en el histórico
    git commit -m "[descriptive message]"

5. Subir los cambios locales al repositorio remoto
    git push [alias] [branch]
```

- Ref:
    - https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf

-->

## Pregunta

> **¿Quién lanza entonces una build y posterior despliegue?**


#

## (Micro)servicios, componentes y repositorios

Controversia: dada una compañía...

* ... con muchos proyectos / aplicaciones / servicios
* ... con varios microservicios / librerías por proyecto / aplicación / servicio, 
    * y dependencias entre ellos,
* ... y código de diferente naturaleza (scripts, configuraciones, aplicación...)

> **¿Cómo se agrupa el código en un sistema de control de versiones?**

## Monorepositorio

<!-- https://developer.atlassian.com/blog/2015/10/monorepos-in-git/ -->
<!-- https://danluu.com/monorepo/ -->

* **Facilita el testing** de unidades interrelacionadas
    * El cambio en un componente lanza la suite de tests que comprueba también las dependencias
* **Cambios que afecten a más de un servicio** o componente **se propagan en un mismo commit**
* **Minimiza problemas de versioning** 
    * Los componentes desplegan de master, y este punto de coherencia es común al resto de componentes
* Google es uno de los ejemplos típicos de uso de esta aproximación, y no el único (compañías como Facebook empezaron con este modelo)...
    * ... pero, en muchos casos, acaban acarreando problemas
    
<!-- https://cacm.acm.org/magazines/2016/7/204032-why-google-stores-billions-of-lines-of-code-in-a-single-repository/fulltext -->

## Multirepositorio

* Se crean **repositorios por propósito** (nota: un repositorio puede generar más de un paquete relacionado)
    * No es buena práctica que un repositorio contenga código de más de un servicio
    * Cuidado con repositorios del tipo "utils"
* Servicios como GitHub soportan también el concepto de organizaciones (minimizando overhead de gestión de permisos)
* Eventualmente, también puede ser necesario **"mezclar" más de un lenguaje en un mismo repositorio**
    * ¿Código asociado a la infraestructura? ¿Y a los tests?
* Los servicios y componentes maduros tienen también **contratos maduros con el resto del mundo**
    * Dichos contratos (APIs) son testeables, en lugar de testear cada una de las integraciones
    * Esto debería facilitar y incrementar confianza con la gestión de dependencias 
