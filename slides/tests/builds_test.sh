#!/bin/bash

set -xe

# default global_opts

exec_root_dir=$(dirname "$0")/..
build=bin/build.sh
build_docker=bin/build-docker.sh

# helpers

function exec_builds {
	$build "$@"
	$build_docker "$@"
}

# tests()

cd $exec_root_dir
rm -f html/*

# executing builds from root
exec_builds $(ls -d src/* | head -1)
exec_builds

# executing a build from a non_root dir
cd $(ls -d src/* | head -1)
../../$build_docker .

echo "INFO: Tests succeeded"
