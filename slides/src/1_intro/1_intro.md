# DevOps como alternativa a la gestión de la Infraestructura IT

#

## Contenidos

| |
| --- |
| Operaciones vs Desarrollo |
| Historia desde el punto de vista de Operaciones |
| Historia desde el punto de vista de Desarrollo |
| DevOps: La aproximación vencedora |
| Algunos gráficos que dan para pensar |

#

## Operaciones vs Desarrollo 

> La batalla que parecía interminable...

![](matrixpills.jpg)

* Dos tipos de perfiles
* Dos tipos de retos
