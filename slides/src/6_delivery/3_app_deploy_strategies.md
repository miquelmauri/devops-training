#

## Estrategias de despliegue de aplicaciones

* Rolling upgrades
* Blue-Green
* Red-Black
* Canary

## Rolling upgrade

![](https://www.paradigmadigital.com/wp-content/uploads/2017/09/despliegue3.png)

<!-- https://www.paradigmadigital.com/dev/estrategias-desplegar-exito-produccion/ -->

## Blue-Green

* Requiere dos versiones concurrentes de la infraestructura, una activa y la otra offline
* Todos los cambios y actualizaciones se hacen sobre la offline, validando constantemente su funcionamiento
* Cuando queremos activar la nueva infraestructura sólo debemos **conmutar de la activa a la offline** y observar durante cierto tiempo (para hacer rollback)
* Finalmente, la versión antigua queda disponible para poder prepara el siguiente cambio

![](https://martinfowler.com/bliki/images/blueGreenDeployment/blue_green_deployments.png){ width=45% }

## Red-Black

* Es la evolución natural de los entornos Blue-Green en infraestructura dinámica
* En lugar de tener una instancia offline entre cambios, cuando necesitamos **creamos la infraestructura dinámicamente**
* Después del cambio, una vez verificado que funciona correctamente, la infraestructura antigua se destruye

## Canary

* Los métodos anteriores aumentan de complejidad con la escala de los sistemas
    * ¿Creéis que Google o Facebook pueden duplicar su infraestructura sólo para cambios?
* La práctica de Canary Release implica desplegar la nueva versión en paralelo con la antigua
* El **despliegue es porcentualmente pequeño (1-10%)** y se distribuye el tráfico para observar su comportamiento en activo
* A medida que se gana confianza las instancias antiguas van siendo reemplazadas por nuevas hasta que se completa el cambio
* ... o se descubre un problema y se hace rollback completo a la versión anterior

![](https://martinfowler.com/bliki/images/canaryRelease/canary-release-2.png)

## Dark Launching / Feature Toggle

* A diferencia de los anteriores, está práctica **oculta** las nuevas funcionalidades a los humanos
* Se automatizan scripts para validar la funcionalidades ocultas a escala global para ver su comportamiento e impacto
* Si se descubre algún problema con el servicio en producción se desactiva la funcionalidad oculta

## ¿Cómo realizamos el routing de peticiones?

* Usando un balanceador de carga, excluimos las instancias nuevas hasta que sea oportuno
    * Esto no proporciona una transición suave entre servicios
* Utilizando un Router de nivel 7 (entiende URLs, tokens, cookies...) que deriva el tráfico a balanceadores de carga actuales y nuevos
    * Esto permite implementar balanceos parciales e implementar Canary Release
* Los parámetros para decidir el destino pueden ser varios (aleatorio, basados en IP o explícitos)
* Finalmente, mediante Cookies podemos asegurar que las sesiones se mantienen en las instancias 
   
## ¿Cómo tratamos los datos?

* Disruptivo: Los datos antiguos deben ser migrados al nuevo esquema
* Progresivo: Los dos esquemas son compatibles mediante versionado
