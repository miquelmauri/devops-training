
#

## Sistemas de control de versiones

VCS Version Control System / SCM Source Code Management

* Trazabilidad de cambios
* Rollback
* Correlación de cambios entre diferentes elementos
* Visibilidad
* Trigger automático de otras acciones (testing)

## ¿Qué guardamos en el sistema de control de versiones?

> Y, por ende, gestionamos como código

* Configuración de infraestructura, políticas, escenarios
    * cookbooks, manifests, playbooks, etc.
* Ficheros de configuración de servicios/aplicaciones y templates
* Código de Test
* Definición de jobs/pipelines de CI y CD
* Scripts auxiliares
* Código fuente de aplicaciones y utilidades
* Documentación

## ¿Qué no suele guardarse en el sistema de control de versiones?

* Los ejecutables, paquetes o librerías deben guardarse en repositorios
* Tampoco los artefactos generados por el propio código
* Datos gestionados por las aplicaciones o ficheros de logs
* Passwords u otros secretos nunca deben guardarse en VCS
    * Al menos deben estar cifrados :)

    
## Git - Actual estándar de facto

```bash
0. Instalar Git

1. Clonar un proyecto
    git clone [url]

2. Verificar los cambios pendientes
    git status

3. Añadir código a versionar
    git add [file]

4. Guardar el código añadido en el histórico
    git commit -m "[descriptive message]"

5. Subir los cambios locales al repositorio remoto
    git push [alias] [branch]
```

Ref:

* [https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf](https://services.github.com/on-demand/downloads/github-git-cheat-sheet.pdf)
