# Práctica 4: Orquestración de Contenedores con Kubernetes

#

## Objetivo

![](https://docs.google.com/drawings/d/e/2PACX-1vT83NPWtF1qS-WpJyDK_ZMmh02aEYwjpqzwgmHkAuXkbSGz1vQ-lpXC__jJkRsgAQ8cDMtITdhP41UY/pub?w=1227&h=552)

## Materiales
```
|-- lab4
|   |-- kubernetes
|   |   |-- app-service.yaml
```

## Requerimientos: Minikube
<!--
Lab based on: https://www.oreilly.com/ideas/how-to-manage-docker-containers-in-kubernetes-with-java?imm_mid=0f75d0&cmp=em-prog-na-na-newsltr_20171021
-->
* Minikube es una herramienta que facilita el deploy de Kubernetes en un nodo único dentro de una VM
* Instalación: https://kubernetes.io/docs/tasks/tools/install-minikube/
    * VirtualBox (en OS X también xhyve driver)
    * Kubectl 
    * Minikube

![](https://cdn-images-1.medium.com/max/750/1*jzcZM6fFBwSjeIIU2roeHw.jpeg){ width=50% }

## Arrancamos Minikube
```bash
$ minikube start
Starting local Kubernetes v1.7.5 cluster...
Starting VM...
Getting VM IP address...
Moving files into cluster...
Setting up certs...
Connecting to cluster...
Setting up kubeconfig...
Starting cluster components...
Kubectl is now configured to use the cluster.

$ kubectl cluster-info
Kubernetes master is running at https://192.168.99.101:8443
```


