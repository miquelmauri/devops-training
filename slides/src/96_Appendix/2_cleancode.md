
## Clean code 

Conjunto de guías, recomendaciones y convenios orientados a **mejorar la legibilidad del código**

> *"Code is read much more often than it is written, so plan accordingly", Raymond Chen*

<!-- Quote by Raymond Chen, https://blogs.msdn.microsoft.com/oldnewthing/20070406-00/?p=27343 -->

## Genera código de calidad desde el primer momento

* Dejarlo para **"después" ralentiza** el proceso de desarrollo...
    * ... y el después se puede convertir en un "nunca"

* Mala calidad de código es uno de los mayores generadores de **deuda técnica**...
    * ... y la deuda técnica se acaba pagando

<!-- Robert C Martin -> Uncle Bob: http://alvinalexander.com/programming/clean-code-quotes-robert-c-martin -->
<!--    Clean Code: A Handbook of Agile Software Craftsmanship -->
<!--    http://www.itiseezee.com/?p=83 -->
<!--    https://www.42lines.net/2012/07/06/clean-code-reducing-wtfs-per-minute/index.html -->

## Prácticas destacables (1): Nombrado

Dar nombre a las diferentes entidades (Variables, Funciones, Clases, Módulos...) puede ser de los problemas más complicados al programar:

* Debe describir la intención
* Debe ser claro (nada de siglas, codificar información...)
* Nombres pronunciables
* Las clases deben ser nombres vs los métodos, que deben ser verbos
* Evita nombres "wildcards"

## Prácticas destacables (2): Uso de funciones, clases, librerías 

Las funciones / clases / módulos permiten agrupar el código en unidades lógicamente relacionadas, consiguiendo:

* Facilitar la creación de tests
* Mejor comprensión de la lógica del programa
* Evitar duplicación de código
* Incrementar la reusabilidad

> *¡Recuerda evitar un número de parámetros masivo!*

## Prácticas destacables (3): El tamaño sí importa

Mantén un numero bajo de líneas:

* por función (<20 líneas)
* por clase / fichero (<1000 líneas)

## Prácticas destacables (4): Comentarios

Aunque sea habitual pensar "muchos comentarios ayudan a entender el texto"...
 
> **Un buen código NO necesita comentarios para ser entendido**

> ¡Referencias a IDs del tracker pueden quedar en el control de versiones!

## Prácticas destacables (5): ¡Evita zombies!

![](http://i.ebayimg.com/00/s/NTAwWDUwMA==/z/Xg8AAOSwhcJWLPEA/$_35.JPG)

## Prácticas destacables (6): Gestión de errores

* Fail fast
* No captures "wildcards"
* Genera errores específicos
* Logea la presencia de errores; no intentes gestionar lo ingestionable

## Ejemplo: Jerarquía de Errores

<!-- https://www.toptal.com/abap/clean-code-and-the-art-of-exception-handling -->
<div id="left">
```ruby
class ApplicationError < StandardError; end

# Validation Errors
class ValidationError < ApplicationError; end
class RequiredFieldError < ValidationError; end
class UniqueFieldError < ValidationError; end

# HTTP 4XX Response Errors
class ResponseError < ApplicationError; end
class BadRequestError < ResponseError; end
class UnauthorizedError < ResponseError; end
```
</div>
<div id="right">
![](https://uploads.toptal.io/blog/image/92349/toptal-blog-image-1460406446134-4d02e7ec15eba7dfd24a91f74d33c732.jpg)
</div>

## ¿Qué es más claro?

```ruby
rescue ValidationError => e
```

```ruby
rescue RequiredFieldError, UniqueFieldError, ... => e
```
