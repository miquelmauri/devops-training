# Infraestructura como Código

#

## Contenidos

|   |
| --- |
| Sistemas de control de versiones |
| Patrones para definir la infraestructura |
| Como definir infraestructura como código |
| Empaquetado de aplicaciones |


## ¿Por qué debemos usar código para gestionar la infraestructura?

* Permite **reusar** la infraestructura en múltiples entornos, garantizando **uniformidad y fiabilidad**
* Mejora el **mantenimiento, auditoría, la colaboración y reduce los tiempos de propagación** 
de cambios mediante la integración con CI/CD
* Facilita la **extensión e integración** con otras partes
* ¡Permite **testear**!
* Desbloquea la **gestión dinámica** de capacidad y la **resolución automática** de problemas
