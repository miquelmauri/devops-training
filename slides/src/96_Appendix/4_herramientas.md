#

## Herramientas para la gestión de código/aplicaciones

## Build 

* Ant
* Maven
* Gradle

## CI

* Jenkins
* Travis
* Team City

## Code Quality

* SonarQube

## Baking
 
* Docker build
* Packer

## Software repositories
<!-- https://binary-repositories-comparison.github.io/ -->
* JFrog Artifactory
* Nexus 


## Code Pipelines

* Jenkins
* GoCD
* Spinnaker
* AWS CodeDeploy
