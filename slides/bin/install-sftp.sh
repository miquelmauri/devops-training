#!/bin/bash

function configureSshKeepalive() {
	cat <<EOF >> ~/.ssh/config
Host *
	ServerAliveInterval 60
	ServerAliveCountMax 4
EOF
}

# main()
export DEBIAN_FRONTEND=noninteractive
apt-get update
apt-get -y install openssh-client

configureSshKeepalive
