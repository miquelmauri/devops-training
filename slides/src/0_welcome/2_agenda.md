#

## Agenda

## 15-16 de Febrero

| Hora | Sección |
| --- | --------- |
| 17:00-17:45 | Welcome y apertura |
| 17:45-19:00 | DevOps como alternativa a la gestión de infraestructuras |
| 19:00-19:20 | *Break* |
| 19:20-20:10 | Arquitecturas IT |
| 20:10-21:00 | Infraestructuras como Código |
| --- | --------- |
| 9:00-10:30 | Práctica 1: Del código al Cloud Provider |
| 10:30-10:50 | *Break* |
| 10:50-12:20 | Ingeniería de Software para la gestión de infraestructuras |
| 12:20-12:40 | *Break* |
| 12:40-13:30 | Entregando software |
| 13:30-14:00 | Práctica 2 Intro: CI/CD de infraestructura dinámica |

## 22-23 de Febrero

| Hora | Sección |
| ----- | --------- |
| 17:00-18:50 | Práctica 2: Despliegue de infraestructura dinámica |
| 18:50-19:10 | *Break* |
| 19:10-20:30 | Gestión de Infraestructura Dinámica |
| 20:30-21:00 | Práctica 3 Intro: Gestión de firewalls con Ansible |
| --- | --------- |
| 9:00-10:30 | Práctica 3: Gestión de firewalls con Ansible |
| 10:30-10:50 | *Break* |
| 10:50-12:10 | Práctica 4: Despliegue en cluster Kubernetes |
| 12:10-12:30 | *Break* |
| 12:30-13:15 | Ideas para favorecer la adopción de la cultura DevOps |
| 13:15-14:00 | Wrap-up y cierre |
