# Práctica 1: Del código al Cloud Provider

#

## Objetivo

![](https://docs.google.com/drawings/d/128ap-RQGC6z89dZ6Jg379V2-O2uKiNvfLzlV0ZEdZR0/pub?w=960&h=720)

## Primera parte: Preparación del entorno virtual

|   |
| --- |
| Clonar repositorio del curso |
| Instalación VirtualBox y Vagrant |
| Arrancar el entorno virtual de prácticas |

## Segunda parte: Definición de IaC y despliegue con aplicación

|   |
| --- |
| Empaquetar nuestra aplicación en un Contenedor Docker |
| Subir nuestro Contenedor a un repositorio |
| Definir nuestra infraestructura en código en AWS |
| Desplegar y validar la infraestructura definida |
| Validación del escenario en producción |


## Tercera parte: Modificaciones extras

|   |
| --- |
| Modifiquemos nuestra IaC |
| ¡Destruyamos la infraestructura! |
