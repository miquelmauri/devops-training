#

##  Herramientas para IaC

<!-- iac_ecosystem.png -->
<!-- https://martinfowler.com/bliki/PhoenixServer.html -->

## Ecosistema para Infraestructura

| Herramienta | Phoenix (Inmutable) | Snowflake (Estado deseado) |
| ------ | ------- | ------- |
| **Deploy** | - | Cambios config (AWS Cloudformation, Terraform) - Ejecución remota (Ansible) |
| **Runtime** | - | Cambios config (Terraform) - Ejecución remota (Ansible) |

## Ecosistema para Sistemas

| Herramienta | Phoenix (Inmutable) | Snowflake (Estado deseado) |
| ------ | ------- | ------- |
| **Base** | Images (ISO, AMI, BOX etc.) - Tools (Packer, Vagrant, VeeWee) | - |
| **Deploy** | Orquestración (Mesos, AWS ASG, Vagrant) - PXE (Cobbler, FAI, Kickstart) | Provisión (Cloud-init, Cfn-init, KickStart) |
| **Runtime** | - | Cambios config (Chef, Puppet, CFEngine) - Ejecución remota (Ansible, Saltstack) |

## Ecosistema para Aplicaciones

| Herramienta | Phoenix (Inmutable) | Snowflake (Estado deseado) |
| ------ | ------- | ------- |
| **Base** | Container Images (Docker) - Tools (Packer, Dockerfile) | - |
| **Deploy** | Orquestración (Mesos, Kubernetes) - Ejecución remota (Ansible, Saltstack) | Ejecución remota (Fabric, Capristrano, Saltstack, Ansible) |
| **Runtime** | - | Cambios config (Chef, Puppet, CFEngine) - Ejecución remota (Ansible, Saltstack) |


## Imperativo vs Declarativo

![](imperative-vs-declarative-xkcd.png){ width=120% }

<!-- ## Arquitectura Agent vs Agent-less -->

<!-- https://www.slideshare.net/MartinEtmajer/automated-deployments-with-ansible -->


## Herramientas para gestión de configuración en servidores
 
| Herramienta | Método | Aproximación |
| ------ | ------- | ------- |
| CFEngine | Pull | Declarativo |
| Chef | Pull | Imperativo |
| Puppet | Pull | Declarativo |
| SaltStack | Push | Declarativo e Imperativo |
| Ansible | Push | Declarativo e Imperativo |

## Popularidad

![](googletrends.png){ width=80% }
