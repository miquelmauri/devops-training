# Práctica 2: CI/CD de infraestructura dinámica

#

## Objetivo

![](https://docs.google.com/drawings/d/1-rk-dMDChhNxcUKdNeGxTLaKcrCqSZdO48Vb_oSW-0o/pub?w=1080&h=423)

## Materiales
```
|-- app
|   |-- Dockerfile
|   |-- app.py
|   |-- app.pyc
|   |-- requirements.txt
|   `-- test_app.py
|-- bitbucket-pipelines.yml
|-- cloudformation
|   |-- cfn_stack_update.py
|   |-- requirements.txt
|   `-- stack_asg.py
`-- scripts
    |-- bootstrap
    |-- cf_stack.sh
    |-- cibuild
    |-- cleanup
    |-- test_docker.sh
    |-- test_unit.sh
    `-- upload_docker_hub.sh
```

## Primera parte: Trabajando con nuestro propio repositorio

|   |
| --- |
| Nuevo repositorio |
| Creando e instalando un par de claves |
| Inicializando el repositorio dentro de la máquina virtual |

## Segunda parte: Testing en local

|   |
| --- |
| Unit Testing |
| Functional Testing |
| Mejorar los tests |

## Tercera parte: Integración y despliegue continuo

|   |
| --- |
| Creación de una Pipeline |
| Definición de pasos y variables |
| Ejecución de la Pipeline |
| Validación de resultados |
| Añadir nuevos pasos a la Pipeline |


