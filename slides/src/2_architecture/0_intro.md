# Arquitecturas IT

#

## Contenidos

|   |
| --- |
| IaC y Cloud, conceptos |
| Arquitectura de aplicaciones: Monolítico vs Microservicios |
| Arquitectura de servicios en Internet |

## Objetivo

Ver los **ingredientes**, en forma de **cambios de diseño y tecnológicos**, que hacen posible 
e influencian a la cultura y forma de trabajar **DevOps**
