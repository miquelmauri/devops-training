% Gestión DevOps de Arquitecturas IT
% Christian Adell & Daniel Caballero
% 15-16 y 22-23 de Febrero, 2019

#

## ¡Bienvenidos!

| Christian Adell | Daniel Caballero |
| :------: | :-------: |
| ![](christian.JPG){ width=25% } | ![](dani.png){ width=25% } |
| chadell(at)gmail(dot)com | dani(dot)caba(at)gmail(dot)com |
| Linkedin [christianadell](https://www.linkedin.com/in/christianadell/) | Linkedin [danicaba](https://www.linkedin.com/in/danicaba) |
| Twitter [chadell0](https://twitter.com/chadell0) |  |


## Ahora vosotros :)

Tendrás 2 minutos para describirnos:

* Tu trayectoria profesional
* ¿Qué sabes sobre DevOps?
* Retos actuales en tu carrera
* Qué esperas del curso
* ¡Algo extraprofesional!

<!--

**¡Apoyándote en una slide!**

## Ejercicio 1

A modo de primer ejercicio, sube tu slide a:

```
sftp://devops-slides-ci:${password}@devops-training.duckdns.org:44044/presentaciones/
```

En formato markdown:

* un fichero "tuNombre.md",
* con un contenido tal y como:

```
## Mi nombre

* Actualmente trabajo en / me estoy preparando para
* [url-linkedin](url-linkedin)
* ...
```
-->
