# Gestión de infraestructura dinámica

#

## Contenidos

|   |
| --- |
| Gestión de la configuración de servicios e Infraestructura |
| Herramientas para IaC |
| Ejemplos de uso de herramientas |
| Continuidad de servicio con Infraestructura dinámica |
| Monitoring |
