# 

## 12-Factor App

12-Factor App es un manifesto que ayuda a construir software para ser entregado como un servicio: web-apps o SaaS. 

Más información en [https://12factor.net/](https://12factor.net/)

## I. Código base (Codebase)
**Un código base sobre el que hacer el control de versiones (por ejemplo Git) y múltiples despliegues.**

Siempre hay una correlación **1:1** entre el repositorio y la aplicación:

* Si hay más de un repo, no es un aplicación, és un sistema distribuido
* No es correcto reusar el mismo código en diferentes aplicaciones, esto debe ser tratado mediante librerias y dependencias

![](https://12factor.net/images/codebase-deploys.png)

## II. Dependencias
**Declarar y aislar explícitamente las dependencias**

Todos los lenguajes de programación ofrecen sistemas de distribución de librerías y estás pueden ir asociadas al SO o a la aplicación.

Nuestras aplicaciones nunca deben asumir la existencia implícita de sus dependencias, deben declararlas y a poder ser aislar su entorno

Por ejemplo, en Python:

* Requirements.txt: Definir los paquetes (y versión) necesarios para la aplicación
* Virtualenv: Creación de entornos Python aislados

## III. Configuraciones
**Guardar la configuración en el entorno**

Las aplicaciones no deben guardar nunca la configuración de entorno (deploy) junto con el código

Esta configuración debe guardarse en variables de entorno, que cambian entre entornos de despliegue

## IV. Backing services
**Tratar a los “backing services” como recursos conectables**

Un "backing service" es cualquier servicio que la aplicación consume, como por ejemplo una base de datos, un sistema de colas, etc., independientemente que sea un servicio local o de un 3rd party.

![](https://12factor.net/images/attached-resources.png){ width=70% }

## V. Construir, desplegar, ejecutar
**Separar completamente la etapa de construcción de la etapa de ejecución**

El código se transforma en un despliegue en tres fases (claramente separadas):

1. **Build**: Transformar el código en un ejecutable añadiendo sus dependencias (librerías)
2. **Release**: Se recoge el *build* y se le añade la configuración de entorno
3. **Run**: La aplicación corre en el entorno de ejecución, arrancando procesos de la aplicación

![](https://12factor.net/images/release.png)

## VI. Procesos
**Ejecutar la aplicación como uno o más procesos sin estado**

La aplicación corre como uno o varios procesos o threads concurrentes, dando lugar a la escalabilidad horizontal.

Estos procesos no tienen estado y no deben compartir nada entre ellos. Cualquier dato que deba persistir deberá ser guardado en un servicio externo (backing-services)

## VII. Asignación de puertos
**Publicar servicios mediante asignación de puertos**

La aplicación debe asociarse a un puerto, que puede no ser el puerto de consumo final. Esto dependerá del entorno final de ejecución (usando diferentes sistemas de routing)

![](https://www.mikelangelo-project.eu/wp-content/uploads/2015/09/vagrant-docker-post.png)

## VIII. Concurrencia
**Escalar mediante el modelo de procesos** (sistema Unix de gestión de procesos), apoyándose en el sistema de procesos del SO

![](https://12factor.net/images/process-types.png)

## IX. Desechabilidad
**Hacer el sistema más robusto intentando conseguir inicios rápidos y graceful shutdowns**

Los procesos deben poder ser arrancados o parados en cualquier momento, para facilitar el escalado y el despliegue de cambios de cógido o configuración

Por tanto, deberemos:

* Minimizar el tiempo de arranque
* Proporcionar *graceful shutdown*, controlado (SIGTERM) e imprevisto

## X. Paridad en desarrollo y producción
**Mantener desarrollo, preproducción y producción tan parecidos como sea posible**

El manifesto pretende minimizar la diferencia entre los entorno mediante la aplicación de *continuous deployment*

![](12factortable.png)

## XI. Logs
**Tratar los historiales como una transmisión de eventos**

Los **logs** son un flujo de eventos agregados y ordenados temporalmente, que son la salida de los diferentes procesos y servicios conectados.

No debemos preocuparnos de guardar o distribuir nuestros eventos. Sólo redirigirlos hacia **stdout** y en cada entorno se puede aplicar diferentes estrategias

## XII. Administración de procesos
**Ejecutar las tareas de gestión/administración como procesos que sólo se ejecutan una vez**

Estos procesos puntuales (por ejemplo creación de la estructura de base de datos) deben estar incluidos en el código de la aplicación

## Objetivos 12-App

* Usan formatos **declarativos** para la automatización de la configuración, para asi minimizar el tiempo/coste que supone unir nuevos desarrolladores al proyecto
* Tienen un **contrato claro** con el sistema operativo sobre el que trabajan, ofreciendo la **máxima portabilidad** entre los diferentes entornos de ejecución
* Son apropiadas para **desplegarse** en **entornos Cloud**, obviando la necesidad de servidores y administración de sistemas
* **Minimizan las diferencias** entre los entornos de desarrollo y producción, posibilitando un **despliegue continuo** para conseguir la máxima agilidad
* Y pueden **escalar** sin cambios significativos en las herramientas, la arquitectura o las prácticas de desarrollo.
