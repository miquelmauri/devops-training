# Gestión DevOps de Arquitecturas IT 

Repositorio con todos los materiales usados en este curso

## Slides

Pueden encontrar las fuentes y html de los slides usados durante el curso. Ver */slides* para más información

## Prácticas

Pueden encontrar el enunciado de las prácticas como parte de las slides

Los materiales necesarios para progresar con éstas están en los directorios */labN* 

