#

## Design patterns

## ¿Qué son los patrones de diseño?

"Recetas" reusables ante problemas recurrentes de Ingeniería del Software.

## Tipos

Actualmente se conocen decenas de patrones de diseño, muchos de ellos recopilados en el 1994 por *"Design Patterns:
Elements of Reusable: Object-Oriented Software"*. Tradicionalmente se clasifican por:

* Patrones creacionales
* Patrones estructurales
* Patrones de comportamiento
* Patrones de concurrencia

## Ejemplo destacable

Wrapper/adapter, como patrón estructural

![](https://obsoletedeveloper.files.wordpress.com/2012/09/hf-adapter.jpg){ width=130% }

## 

Nos indica que:

* ante un problema de compatibilidad entre cliente e interfaz (librería, api), es mejor generar un interfaz que conecte ambas piezas, haciendo de puente (wrapper)

El rationale detrás:

* Reusabilidad
* Número de cambios

##
<!-- https://sourcemaking.com/design_patterns/adapter/python/1 -->
```python
class Target(metaclass=abc.ABCMeta):
    """
    Define the domain-specific interface that Client uses.
    """
    def __init__(self):
        self._adaptee = Adaptee()

    @abc.abstractmethod
    def request(self):
        pass


class Adapter(Target):
    """
    Adapt the interface of Adaptee to the Target interface.
    """
    def request(self):
        self._adaptee.specific_request()


class Adaptee:
    """
    Define an existing interface that needs adapting.
    """
    def specific_request(self):
        pass


def main():
    adapter = Adapter()
    adapter.request()


if __name__ == "__main__":
    main()
```
