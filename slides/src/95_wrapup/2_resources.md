#

## Para aprender más...

## Libros

* "The Phoenix Project", by Gene Kim, Kevin Behr, George Spafford
* "Infrastructure as Code", by Kief Morris, Junio 2016
* "Building Microservices", by Sam Newman, Febrero 2015
* "The DevOps 2.0 Toolkit", by Viktor Farcic, Mayo 2017
* "Clean Code: A Handbook of Agile Software Craftsmanship", by Robert C. Martin
* "Continuous Delivery: Reliable Software Releases through Build, Test, and Deployment Automation, 
by Jez Humble and David Farley
* "Site Reliability Engineering", by *Google* 

## Webs y otras publicaciones

* [https://martinfowler.com/](https://martinfowler.com/)
* [http://highscalability.com/](http://highscalability.com/)
* [https://12factor.net/](https://12factor.net/)
* [http://www.dasblinkenlichten.com/](http://www.dasblinkenlichten.com/)
* Devops/SRE weekly newsletters
